package com.example.getpost;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;

import java.io.IOException;

import okhttp3.Call;
import okhttp3.FormBody;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class MainActivity extends AppCompatActivity {

    Button buttonGetById, buttonGetAllPersons, buttonSavePerson;
    EditText editTextId, editTextFirstName, editTextLastName;
    TextView textView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        buttonGetById = (Button) findViewById(R.id.buttonGet);
        buttonSavePerson = (Button) findViewById(R.id.buttonSavePerson);
        buttonGetAllPersons = (Button) findViewById(R.id.buttonGet2);
        editTextId = (EditText) findViewById(R.id.editTextId);
        editTextFirstName = (EditText) findViewById(R.id.editTextFirstName);
        editTextLastName = (EditText) findViewById(R.id.editTextLastName);
        textView = (TextView) findViewById(R.id.textView);

        buttonGetById.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String URL = "http://10.0.2.2:8080/person/byId/" + editTextId.getText().toString();


                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        Request request = new Request.Builder()
                                .url(URL)
                                .get()
                                .build();

                        OkHttpClient client = new OkHttpClient();
                        Call call = client.newCall(request);

                        Response response;
                        try {
                            response = call.execute();
                            String serverResponse = response.body().string();
                            ObjectMapper mapper = new ObjectMapper();
                            Person person = mapper.readValue(serverResponse, Person.class);
//                            Person[] persons = mapper.readValue(serverResponse, Person[].class);
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    StringBuilder stringBuilder = new StringBuilder();
                                    stringBuilder.append("id: " + person.getId() + "\n");
                                    stringBuilder.append("firstName: " + person.getFirstName() + "\n");
                                    stringBuilder.append("lastName: " + person.getLastName());
                                    textView.setText(stringBuilder.toString());
                                }
                            });
                        } catch (Exception e) {
                            throw new RuntimeException(e);
                        }
                    }
                }).start();
            }
        });

        buttonGetAllPersons.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String URL = "http://10.0.2.2:8080/person/all";
                String firstName = editTextFirstName.getText().toString();
                String lastName = editTextLastName.getText().toString();

                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        Request request = new Request.Builder()
                                .url(URL)
                                .get()
                                .build();
                        OkHttpClient client = new OkHttpClient();
                        Call call = client.newCall(request);

                        Response response;
                        try {
                            response = call.execute();
                            String serverResponse = response.body().string();
                            ObjectMapper mapper = new ObjectMapper();
                            Person[] persons = mapper.readValue(serverResponse, Person[].class);
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    StringBuilder stringBuilder = new StringBuilder();
                                    for (Person p : persons) {
                                        stringBuilder.append("id: ").append(p.getId()).append("\n");
                                        stringBuilder.append("firstName: " + p.getFirstName() + "\n");
                                        stringBuilder.append("lastName: " + p.getLastName() + "\n" + "\n");
                                    }
                                    textView.setText(stringBuilder.toString());
                                }
                            });

                        } catch (IOException e) {
                            throw new RuntimeException(e);
                        }

                    }
                }).start();

            }
        });

        buttonSavePerson.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String URL = "http://10.0.2.2:8080/person/save";
                String firstName = editTextFirstName.getText().toString();
                String lastName = editTextLastName.getText().toString();

                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        Person person = new Person();
                        person.setLastName(lastName);
                        person.setFirstName(firstName);

                        ObjectWriter objectMapper = new ObjectMapper().writer();
                        MediaType mediaType = MediaType.get("application/json");
                        RequestBody requestBody;
                        try {
                            requestBody = RequestBody.create(objectMapper.writeValueAsString(person), mediaType);
                        } catch (JsonProcessingException e) {
                            throw new RuntimeException(e);
                        }
                        Request request = new Request.Builder()
                                .url(URL)
                                .post(requestBody)
                                .build();
                        OkHttpClient client = new OkHttpClient();
                        Call call = client.newCall(request);

                        Response response;
                        try {
                            response = call.execute();
                            String serverResponse = response.body().string();
                            ObjectMapper mapper = new ObjectMapper();
                            Person personResponse = mapper.readValue(serverResponse, Person.class);
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    StringBuilder stringBuilder = new StringBuilder();
                                    stringBuilder.append("id: ").append(personResponse.getId()).append("\n");
                                    stringBuilder.append("firstName: " + personResponse.getFirstName() + "\n");
                                    stringBuilder.append("lastName: " + personResponse.getLastName() + "\n" + "\n");
                                    textView.setText(stringBuilder.toString());
                                }
                            });

                        } catch (IOException e) {
                            throw new RuntimeException(e);
                        }

                    }
                }).start();
            }
        });


//        buttonGet2.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                String URL = "https://reqres.in/api/unknown";
//
//                new Thread(new Runnable() {
//                    @Override
//                    public void run() {
//                        Request request = new Request.Builder()
//                                .url(URL)
//                                .get()
//                                .build();
//
//                        OkHttpClient client = new OkHttpClient();
//                        Call call = client.newCall(request);
//
//                        Response response;
//                        try {
//                            response = call.execute();
//                            String serverResponse = response.body().string();
//
//                            runOnUiThread(new Runnable() {
//                                @Override
//                                public void run() {
//
//                                }
//                            });
//                        } catch (Exception e) {
//                            throw new RuntimeException(e);
//                        }
//                    }
//                }).start();
//            }
//        });
    }
}